export class RegistroModel{

    id: string;
    iduserId: string;
    title: string;
    completed: boolean;

    constructor(){
        this.completed = true;
    }

}