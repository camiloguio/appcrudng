import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { RegistroModel } from '../../models/registro.model';
import { RegistrosService } from '../../services/registros.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html'
})
export class RegistroComponent implements OnInit {

  /* Creando una nueva propiedad */

  registro = new RegistroModel();
  
  constructor(private registrosService:RegistrosService,
              private route:ActivatedRoute) { }

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id')

    if (id !== 'nuevo') {
      this.registrosService.getregistro( id )
      .subscribe((resp:RegistroModel) => {
        this.registro = resp
        this.registro.id = id
      } )
    }
    

  }

  guardar( form: NgForm ){

    if (form.invalid) {
    console.log('Formulario no valido')
    return;
    }

    Swal.fire({
      title: 'Espere',
      text: 'Guardando información',
      type: 'info',
      allowOutsideClick: false
    });
    Swal.showLoading();

    let peticion: Observable<any>;

    if (this.registro.id) {
      peticion = this.registrosService.actualizarRegistro(this.registro)
      
    } else {
      peticion = this.registrosService.crearRegistro(this.registro)
    
    }
    
    peticion.subscribe( resp => {

      Swal.fire({
        title: this.registro.iduserId,
        text: 'Se actualizó Correctamente',
        type: 'success'
      })
      
    });
  }

}
