import { Component, OnInit } from '@angular/core';
import { RegistrosService } from '../../services/registros.service';
import { RegistroModel } from '../../models/registro.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registros',
  templateUrl: './registros.component.html'
})
export class RegistrosComponent implements OnInit {

  registros: RegistroModel[] = [];
  cargando = false;

  constructor(private registrosService:RegistrosService) { }

  ngOnInit() {

    this.cargando = true;
    
    Swal.fire({
      title: 'Cargando Información',
      timer: 1000,
      onBeforeOpen: () => {
      Swal.showLoading()
      }
    })

    this.registrosService.getRegistros()
    .subscribe( resp => {
      this.registros = resp
      this.cargando = false
    })
            
  }

  borrarRegistro(registro:RegistroModel, i:number){

    Swal.fire({
      title: '¿Está seguro?',
      text: 'Esta acción no se puede deshacer',
      type: 'question',
      showConfirmButton: true,
      showCancelButton: true
    }).then(resp => {
      if(resp.value){
        this.registros.splice(i, 1)
        this.registrosService.borrarRegistro(registro.id).subscribe();
      }
    })

    
  }
}
