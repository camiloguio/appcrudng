import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrosComponent } from './pages/registros/registros.component';
import { RegistroComponent } from './pages/registro/registro.component';

const routes: Routes = [
  { path: 'registros', component: RegistrosComponent},
  { path: 'registro/:id', component: RegistroComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'registros' }
]

@NgModule({
  imports: [
    RouterModule.forRoot( routes )
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
