import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegistroModel } from '../models/registro.model';
import { map } from 'rxjs/Operators';

@Injectable({
  providedIn: 'root'
})
export class RegistrosService {

  private url = 'https://appcrudng.firebaseio.com'

  constructor(private http: HttpClient) { }

  crearRegistro( registro: RegistroModel ){
    return this.http.post(`${this.url}/registros.json`, registro)
    .pipe(
      map((resp:any) => {
          registro.id = resp.name;
          return registro;
      })
    )
  }  

  actualizarRegistro( registro: RegistroModel ){
    const registroTemp = {
      ...registro
    };

    delete registroTemp.id; 

    return this.http.put(`${this.url}/registros/${registro.id}.json`, registroTemp);
  }

  borrarRegistro(id: string){
    return this.http.delete(`${this.url}/registros/${id}.json`)
  }

  getregistro(id: string){
    return this.http.get(`${this.url}/registros/${id}.json`)
  }

  getRegistros(){
    return this.http.get(`${this.url}/registros.json`)
      .pipe(
        map(this.crearArreglo)
      )
  }
  
  buscarRegistros(){    
      return this.http.get(`${this.url}/registros.json`)
        .pipe(
          map(this.crearArreglo)
        )
  }

  private crearArreglo( registrosObj: object ){
      
    const registros: RegistroModel[] = []
    
    /* Validando si no hay información en el arreglo */

    if( registrosObj === null ) { return []; }

    Object.keys( registrosObj ).forEach( key =>{
      const registro:RegistroModel = registrosObj[key];
      registro.id = key;

      registros.push(registro)
    } )

      return registros;
  }
  
}
